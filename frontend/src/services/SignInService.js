import HttpClient from './http/HttpClient'
const URL = 'user'
const signIn = async (body) => {
  return await HttpClient.post(`${URL}/signIn`, body)
}
export default {
  signIn
}
