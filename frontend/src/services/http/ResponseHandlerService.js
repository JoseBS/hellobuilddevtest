
import HttpErrorHandlerService from './ErrorHandlerService'

const ResponseHandlerService = async (res) => {
  const { status } = res
  if (status < 400) {
    const body = await res.json()
    return { status, ...body }
  } else {
    HttpErrorHandlerService(res)
    return { status }
  }
}

export default ResponseHandlerService
