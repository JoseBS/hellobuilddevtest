import AlertsService from '../alerts/AlertsService'
const HttpErrorHandlerService = (res) => {
  res.json().then((e) => {
    AlertsService({
      titleText: e.name,
      text: e.message,
      icon: 'error',
      position: 'top-end'
    })
  })
}

export default HttpErrorHandlerService
