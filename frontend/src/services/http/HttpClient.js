import ResponseHandlerService from './ResponseHandlerService'
const API_URL = 'http://localhost:3002/api'
const get = async ({ url }) => {
  // fetch(`${API_URL}/${url}`)
}
const post = async (url, body, headers) => {
  const res = await fetch(`${API_URL}/${url}`, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
      ...headers
    },
    body: JSON.stringify(body)
  })
  return await ResponseHandlerService(res)
}
export default {
  get,
  post
}
