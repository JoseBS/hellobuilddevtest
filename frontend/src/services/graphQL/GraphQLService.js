import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client'

import { setContext } from '@apollo/client/link/context'
import AuthService from '../auth/AuthService'

const httpLink = createHttpLink({
  uri: 'https://api.github.com/graphql',
  fetch
})

const authLink = setContext((_, { headers }) => {
  const token = AuthService.getGitHubToken()
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const GraphQLService = new ApolloClient({
  connectToDevTools: true,
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
  onError: ({ graphQLErrors }) => {
    console.log(graphQLErrors)
  }
})

export default GraphQLService
