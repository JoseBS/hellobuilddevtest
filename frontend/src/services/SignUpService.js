import HttpClient from './http/HttpClient'
const URL = 'user'
const signUp = async (body) => {
  return await HttpClient.post(`${URL}/signUp`, body)
}
export default {
  signUp
}
