import StoreService from '../store/StoreService'
import HttpClient from '../http/HttpClient'
const OWN_TOKEN_KEY = 'ownToken'
const GITHUB_TOKEN_KEY = 'gitHubToken'

const setOwnToken = (token) => {
  StoreService.setData(OWN_TOKEN_KEY, token)
}
const getOwnToken = () => {
  return StoreService.getDataByKey(OWN_TOKEN_KEY)
}
const setGitHubToken = (token) => {
  StoreService.setData(GITHUB_TOKEN_KEY, token)
}
const getGitHubToken = () => {
  return StoreService.getDataByKey(GITHUB_TOKEN_KEY)
}

const setGitHubTokenFromBackend = async (code) => {
  const res = await HttpClient.post('user/gitHubAuth', { code }, {
    authorization: `Bearer ${getOwnToken()}`
  })
  setGitHubToken(res.token)
}
const isAuthenticated = () => getGitHubToken() && getOwnToken()
const logOut = () => {
  StoreService.clearAll()
  window.location.pathname = '/signIn'
}
export default {
  setOwnToken,
  getOwnToken,
  setGitHubToken,
  getGitHubToken,
  setGitHubTokenFromBackend,
  isAuthenticated,
  logOut
}
