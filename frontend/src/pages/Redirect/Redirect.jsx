import { useEffect } from 'react'
import { useSearchParams, useNavigate } from 'react-router-dom'
import AlertService from '../../services/alerts/AlertsService'
import AuthService from '../../services/auth/AuthService'
const Redirect = () => {
  const navigate = useNavigate()
  const [searchParams, setSearchParams] = useSearchParams()
  useEffect(() => {
    if (searchParams.get('error')) {
      AlertService({
        toast: false,
        icon: 'error',
        titleText: 'No auth granted',
        text: 'GitHub authorization rejected by user'
      })
      navigate('/signIn', { replace: true })
    }
    const code = searchParams.get('code')
    if (code) {
      async function fetchData () {
        await AuthService.setGitHubTokenFromBackend(code)
        navigate('/private/home', { replace: true })
      }
      fetchData()
    }
  }, [searchParams, navigate])

  return (<></>)
}
export default Redirect
