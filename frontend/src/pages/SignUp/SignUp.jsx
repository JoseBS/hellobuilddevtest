import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { Link, Navigate } from 'react-router-dom'
import Input from '../../components/Input/Input'
import Button from '../../components/Button/Button'

import './signUp.css'

import SignUpService from '../../services/SignUpService'
import AuthService from '../../services/auth/AuthService'

const signUpFormValidation = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required('No password provided.')
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z][0-9]/, 'Password can only contain Latin letters.'),
  passwordConfirm: Yup.string().required()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
})
const SignUp = () => {
  const handleSubmit = async ({ email, password }) => {
    const res = await SignUpService.signUp({ email, password })
    if (Number(res.status) >= 400) {
      return
    }
    AuthService.getOwnToken(res.token)
    window.location.href = res.authPage
    // console.log({ res })
  }
  if (AuthService.isAuthenticated()) {
    return <Navigate to='/private' />
  }
  return (
    <div className='container'>
      <section className='sec-sign-up'>
        <Formik
          initialValues={{
            email: '',
            password: '',
            passwordConfirm: ''
          }}
          validationSchema={signUpFormValidation}
          onSubmit={handleSubmit}
        >
          {
            ({ isValid, dirty }) => (
              <Form>
                <Input name='email' label='Email' />
                <Input type='password' name='password' label='Password' />
                <Input type='password' name='passwordConfirm' label='Confirmation' />
                <Button type='submit' disabled={!isValid || !dirty}>Sign Up</Button>
              </Form>
            )
          }
        </Formik>
        <p>Already have an account? <Link to='/signIn'>Sign In</Link></p>
      </section>
    </div>
  )
}
export default SignUp
