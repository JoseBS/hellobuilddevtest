import { Formik, Form } from 'formik'
import * as Yup from 'yup'

import Input from '../../components/Input/Input'
import Button from '../../components/Button/Button'

import './SignIn.css'

import SignInService from '../../services/SignInService'
import AuthService from '../../services/auth/AuthService'
import { Link, Navigate } from 'react-router-dom'

const signInFormValidation = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required('No password provided.')
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z][0-9]/, 'Password can only contain Latin letters.')
})
const SignIn = () => {
  const handleSubmit = async ({ email, password }) => {
    const res = await SignInService.signIn({ email, password })
    if (Number(res.status) >= 400) {
      return
    }
    AuthService.setOwnToken(res.token)
    window.location.href = res.authPage
  }
  if (AuthService.isAuthenticated()) {
    return <Navigate to='/private' />
  }
  return (
  // <div className='container'>
    <section className='sec-sign-up'>
      <Formik
        initialValues={{
          email: '',
          password: ''
        }}
        validationSchema={signInFormValidation}
        onSubmit={handleSubmit}
      >
        {
            ({ isValid, dirty }) => (
              <Form>
                <Input name='email' label='Email' />
                <Input type='password' name='password' label='Password' />
                <Button type='submit' disabled={!isValid || !dirty}>Sign In</Button>
              </Form>
            )
          }
      </Formik>
      <p>Don't have an account? <Link to='/signUp'>Sign Up</Link></p>
    </section>
  // </div>
  )
}
export default SignIn
