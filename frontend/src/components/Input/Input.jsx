import { useField } from 'formik'
import { memo } from 'react'
import './Input.css'

const ErrorLabel = ({ children }) => {
  return (<div className='error-label'>{children}</div>)
}

const Input = ({ label, ...props }) => {
  const [field, meta] = useField(props)
  return (
    <div className='input-field'>
      <div className='input-label'>{label}</div>
      <input className='input-item' {...field} {...props} />
      {meta.touched && meta.error && <ErrorLabel>{meta.error}</ErrorLabel>}
    </div>
  )
}
export default memo(Input)
