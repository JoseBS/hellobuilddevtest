import { memo } from 'react'
import './Home.css'

const Home = ({ children }) => {
  const { email, login, avatarUrl, name, repositories } = children
  if (!children) return (<h1>Loading</h1>)
  return (
    <div className='home'>
      <div className='profile'>
        <div className='profile-header'>
          <div className='profile-name'>
            Name: {name}
          </div>
          <img src={avatarUrl} className='profile-pic' alt='NotAvailable' />
        </div>
        <div className='profile-content'>
          <div className='profile-repocount'>
            Repositories: {repositories?.length}
          </div>
          <div className='profile-email'>
            Your email: {email}
          </div>
          <div className='profile-link'>
            GitHub link: <a href={`https://github.com/${login}`} target='_blank' rel='noreferrer'>@{login}</a>
          </div>
        </div>

      </div>
    </div>
  )
}
export default memo(Home)
