import { useState, useEffect, createContext } from 'react'
import RepositoriesList from '../../components/RepositoriesList/RepositoriesList'
import FindRepository from '../../components/FindRepository/FindRepository'
import StoreService from '../../../../services/store/StoreService'
import './Repositories.css'

export const RepositoriesContext = createContext({ repos: [], toggleFav: () => {} })
const { Provider: RepositoriesProvider } = RepositoriesContext

const getOnlyFavorites = (repos) => repos.filter(repo => repo.isFavorite === true)
const isFavoriteRepo = (id) => {
  const favs = StoreService.getDataByKey('favs')
  return favs?.some(fav => fav === id) ?? false
}
const mapRepos = (children) => children?.repositories?.map?.(repo => ({ ...repo, isFavorite: isFavoriteRepo(repo.id) }))
const Repositories = ({ children }) => {
  const [repos, setRepos] = useState(children.repositories)
  const [onlyListFavorites, setOnlyListFavorites] = useState(false)
  const [search, setSearch] = useState('')
  useEffect(() => {
    setRepos(mapRepos(children))
  }, [children])
  useEffect(() => {
    filter()
  }, [onlyListFavorites, search])
  const handleFilter = ({ target }) => {
    setSearch(target.value)
  }
  const handleCheck = ({ target }) => {
    // setOnlyListFavorites((prev) => !prev)
    setOnlyListFavorites(target.checked)
  }
  const filter = () => {
    const searchText = search?.toLowerCase?.()
    if (searchText === null || searchText === '') {
      setRepos(onlyListFavorites ? getOnlyFavorites(mapRepos(children)) : mapRepos(children))
      return
    }
    let newRepos = mapRepos(children)?.filter?.(
      (repo) => {
        return repo?.name?.toLowerCase?.().includes?.(searchText) || repo?.description?.toLowerCase?.().includes?.(searchText)
      }
    )
    if (onlyListFavorites) newRepos = getOnlyFavorites(newRepos)
    setRepos(newRepos)
  }
  const toggleFav = (id) => {
    StoreService.toggleFav(id)
    filter()
  }

  return (
    <RepositoriesProvider value={{ repos, toggleFav }}>
      <div className='repositories'>
        <div className='filter'>
          <FindRepository handleFilter={handleFilter} />
          <label>
            <input
              type='checkbox' checked={onlyListFavorites} onChange={handleCheck}
            />Favorites
          </label>
        </div>
        {/* <RepositoriesList filter={filter}>{repos}</RepositoriesList> */}
        <RepositoriesList />
      </div>
    </RepositoriesProvider>
  )
}
export default Repositories
