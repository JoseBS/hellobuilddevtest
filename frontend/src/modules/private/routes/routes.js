import Home from '../pages/Home/Home.jsx'
import Repositories from '../pages/Repositories/Repositories.jsx'
const PrivateRouting = [{
  to: 'home',
  path: 'home',
  label: 'Home',
  Component: Home,
  index: true,
  isAvailableToNav: true
}, {
  to: 'repositories',
  path: 'repositories',
  label: 'Respositories',
  Component: Repositories,
  isAvailableToNav: true
}]

export default PrivateRouting
