
import { ApolloProvider } from '@apollo/client'
// import StoreService from '../../../services/store/StoreService'
import AuthService from '../../../services/auth/AuthService'
import GraphQLService from '../../../services/graphQL/GraphQLService'
// import Home from '../pages/Home/Home.jsx'
import './Layout.css'
import PrivateRouting from '../routes/routes'
import Header from '../components/Header/Header'
import Content from '../components/Content/Content'
import { Navigate } from 'react-router-dom'

export const LazyLayout = () => {
  if (!AuthService.isAuthenticated()) {
    return <Navigate to='/signIn' />
  }
  return (
    <ApolloProvider client={GraphQLService}>
      <div className='layout'>
        <Header PrivateRouting={PrivateRouting} />
        <Content PrivateRouting={PrivateRouting} />
      </div>
    </ApolloProvider>
  )
}

export default LazyLayout
