import { useContext } from 'react'

import { RepositoriesContext } from '../../pages/Repositories/Repositories'
import RepositoryItem from '../RepositoryItem/RepositoryItem'

import './RepositoriesList.css'

const RepositoriesList = () => {
  const { repos } = useContext(RepositoriesContext)
  if (!repos || repos.length === 0) {
    return (<h1>No repositories found</h1>)
  }
  return (
    <div className='repositories-list'>
      {repos?.map?.((repo) => repo && <RepositoryItem key={repo.name} repo={repo} />
      )}
    </div>
  )
}
export default RepositoriesList
