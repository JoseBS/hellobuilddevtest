
import { useEffect, useState } from 'react'
import { Navigate, Routes, Route } from 'react-router-dom'
import { useQuery, gql } from '@apollo/client'
const QUERY = gql`
{
  viewer {
    avatarUrl
    login
    email
    name
    createdAt
    repositories(first: 100) {
      edges {
        node {
          name
          url
          createdAt
          description
          databaseId
        }
      }
    }
  }
}`
const mapRepos = (repos) => {
  // console.log(repos)
  return repos?.map?.(({ node: repo }) => {
    return {
      name: repo.name,
      description: repo.description,
      url: repo.url,
      id: repo.databaseId
    }
  })
}
const Content = ({ PrivateRouting }) => {
  const [dataChildren, setData] = useState({})
  const { data, loading, error } = useQuery(
    QUERY,
    {
      fetchPolicy: 'cache-and-network'
    }
  )
  useEffect(() => {
    if (!data) {
      return
    }
    setData({
      ...data.viewer,
      repositories: mapRepos(data?.viewer?.repositories?.edges) ?? []
    })
  }, [data])
  if (loading) {
    return (<h1>Loading</h1>)
  }
  if (error) {
    return (<h1>Error</h1>)
  }
  return (
    <Routes>
      {PrivateRouting.map(route => {
        return (<Route path={route.path} key={`route-${route.path}`} element={<route.Component>{dataChildren}</route.Component>} />)
      })}

      {/* <Route path="*" element={ <div>Not Found</div> } /> */}
      <Route path='*' element={<Navigate replace to='home' />} />
    </Routes>
  )
}
export default Content
