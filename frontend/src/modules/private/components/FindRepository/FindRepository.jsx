import './FindRepository.css'
const FindRepository = ({ handleFilter }) => {
  return (
    <label className='find-repository'>Filter:
      <input className='find-repository-input' onKeyUp={handleFilter} />
    </label>
  )
}
export default FindRepository
