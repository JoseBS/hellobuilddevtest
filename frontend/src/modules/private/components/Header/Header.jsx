import { NavLink } from 'react-router-dom'
import AuthService from '../../../../services/auth/AuthService'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import './Header.css'
const Header = ({ PrivateRouting }) => {
  const handleLogout = () => {
    AuthService.logOut()
  }
  return (
    <nav className='header-nav'>
      <ul className='header-ul'>
        {PrivateRouting.map(child => {
          return (child.isAvailableToNav &&
            <li key={`nav-${child.path}`}>
              <NavLink className='header-nav-link' key={`nav-${child.path}`} to={child.to}>
                {child.label}
              </NavLink>
            </li>
          )
        })}
      </ul>
      <button className='logout' type='button' onClick={handleLogout}>
        <FontAwesomeIcon icon={faSignOutAlt} />
        Bye
      </button>
    </nav>
  )
}
export default Header
