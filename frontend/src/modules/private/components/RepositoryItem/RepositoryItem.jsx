import { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart as favIcon } from '@fortawesome/free-solid-svg-icons'

// import StoreService from '../../../../services/store/StoreService'

import { RepositoriesContext } from '../../pages/Repositories/Repositories'

import './RepositoryItem.css'
const Repository = ({ repo }) => {
  const { toggleFav } = useContext(RepositoriesContext)
  if (!repo) {
    return null
  }
  return (
    <div className='repo-item'>
      <div className='repo-info'>
        <div className='repo-label'>{repo?.name ?? 'Name not available'}</div>
        <div className='repo-description'>{repo?.description ?? 'Description not available'}</div>
      </div>
      <div className='repo-fav' onClick={() => toggleFav(repo?.id)}>
        <FontAwesomeIcon icon={favIcon} color={repo?.isFavorite ? 'red' : 'black'} title='Toggle' />
      </div>
    </div>
  )
}

export default Repository
