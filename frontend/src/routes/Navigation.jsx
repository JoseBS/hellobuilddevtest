import { Suspense } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'

import AppRouting from './routes'

export const Navigation = () => {
  return (
    <Suspense fallback={<span>Loading...</span>}>

      <BrowserRouter>
        <Routes>
          {
              AppRouting.map(({ path, Component }) => (
                <Route
                  key={path}
                  path={path}
                  element={<Component />}
                />
              ))
            }
          <Route path='*' element={<Navigate to={AppRouting[0].to} replace />} />
        </Routes>
      </BrowserRouter>
    </Suspense>
  )
}
