import { lazy } from 'react'

import * as Pages from '../pages' // no use this on wide applications that require lazy loaded routes
const PrivateLayout = lazy(() => import('../modules/private/layout/Layout'))
const AppRouting = [
  {
    to: '/signIn',
    path: 'signIn',
    label: 'Sign In',
    Component: Pages.SignIn,
    index: true,
    isAvailableToNav: true
  },
  {
    to: '/private/*',
    path: '/private//*',
    label: '',
    Component: PrivateLayout,
    isAvailableToNav: false
  },
  {
    to: '/signUp',
    path: 'signUp',
    label: 'Sign Up',
    Component: Pages.SignUp,
    isAvailableToNav: true
  }, {
    to: '',
    path: 'redirect',
    label: 'Sign Up',
    Component: Pages.Redirect,
    isAvailableToNav: false
  }
]
export default AppRouting
