import { Navigation } from './routes/Navigation'
import './App.css'
function App () {
  return (
    <div className='app'>
      <Navigation />
    </div>
  )
}

export default App
